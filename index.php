<?php 
 $curl = curl_init();
 curl_setopt($curl, CURLOPT_URL, "http://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=f35fa1598f78451f83c3e055a3539c6a");
 curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
 $output = curl_exec($curl);
 curl_close($curl);

 $data = json_decode($output, true);
?>
   

<?php include 'header.php';?>
<div class="container">
  <h2>Jumlah Kasus di Indonesia Saat Ini</h2>  
  
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="card">
            <h2>Terkonfirmasi</h2>
            <div id=global-kasus class="number" style="color: #ff0000;">-----</div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card">
            <h2>Dalam Perawatan</h2>
            <div id=global-rawat class="number" style="color: #eed202;">-----</div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card">
            <h2>Sembuh</h2>
            <div id=global-pulih class="number" style="color: #32cd32;">-----</div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card">
            <h2>Meninggal</h2>
            <div id=global-meninggal class="number">-----</div>
            </div>
        </div>
        <iframe src="https://experience.arcgis.com/experience/57237ebe9c5b4b1caa1b93e79c920338" allowfullscreen="" width="100%" height="850" frameborder="0"></iframe>
</div>
</div>
<br>
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 bg-danger card4">
							<b>Assalamu'alaikum Wr. Wb.</b><br><br>

							Menghadapi wabah Covid-19 yang semakin hari semakin membuat takut dan menimbulkan rasa tidak aman bagi masyarakat dunia, Pangkep.net turut berpartisipasi dalam menyediakan sistem deteksi dini terhadap serangan Covid-19.<br><br>

							Deteksi dini ini dibuat online yang dapat dimanfaatkan oleh masyarakat umum. Saudara tinggal menjawab beberapa pertanyaan dan hasil jawaban akan dianalisis secara online untuk menduga status saudara termasuk dalam kategori: sehat/bebas Covid-19, ODP (Orang Dalam Pemantauan) atau PDP (Pasien Dalam Pengawasan).<br><br>

							Untuk deteksi dini Covid-19 silahkan <a href="<?=base_url()?>deteksi.php" class="btn btn-xs btn-warning">KLIK DISINI</a><br><br>
							
							<b>Wassalamu'alaikum Wr. Wb.</b><br><br>
							<!-- <b>#Tim Tanggap Covid-19 UMM</b><br /><br /> -->
							
							Call Center COVID-19 Pantar Pangkep:<br>
							(HP/WA) +62 821-9118-2456<br><br><br>
							
							Bagi saudara yang sudah pernah mengisi form deteksi dini Covid-19 dan <b>ingin melihat kembali</b> hasil analisis tanpa input ulang, dapat <a href="hasil.php" class="btn btn-xs btn-info">KLIK DISINI</a>
                                
                            </div>
                            <div class="col-md-6 col-sm-6 card3">
                               <h2>Info Terkini</h2>
                                <div class="some-list-2">
                            <?php foreach($data['articles'] as $d){ ?> 
                            <div class="media card2">
                                <div class="media-left">
                                    <a href="<?php echo $d['url']; ?>">
                                    <img class="media-object" src="<?php echo $d['urlToImage']; ?>" width=100 class="img-responsive" alt="...">
                                    </a>
                                </div> 
                                <div class="media-body">
                                    <h4 class="media-heading"><strong><?php echo $d['title']; ?></strong></h4>
                                    <?php echo $d['description']; ?>
                                    <a href="<?php echo $d['url']; ?>" class="label label-primary" target="_blank">Lihat Detail</a>
                                </div> 
                                
                            </div>
                            <?php } ?>
                            </div>
                            </div>
                        </div>
</div>

<?php include 'footer.php';?>
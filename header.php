<?php require_once 'config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo title;?> - <?php echo description;?> </title>
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-datepicker3.css">
    <link href="https://fonts.googleapis.com/css?family=Kreon&display=swap" rel="stylesheet">

<style>
    body, html {
      font-family: 'Kreon', serif;
    }
h2 {
  text-align: center;
  margin-bottom:50px;
}
h3 {
  margin-bottom:50px;
}
h4 {
  color: #000;
}

.text-title {
    font-size:16px;
    font-weight:bolder;
    margin : 10px 5px 5px 5px;
}
.b-radio {
    padding : 5px 5px 30px 5px;
}
.p-footer {
    font-size:12px;
    padding:25px;
    text-align:center;
    margin-top:50px;
}
.card {
  height: 150px;
  background-color:#f8f8f8;
  padding: 5px 0 0 0;
  -webkit-border-radius: 10px 10px 10px 10px;
border-radius: 10px 10px 10px 10px;
margin-bottom: 20px;

}
.card2 {
  padding-bottom: 20px;
}
.card3 {
  padding: 10px 20px 20px 15px;
}
.card4 {
  padding: 20px 20px 20px 20px;
}
.card .number {
  position: relative;
  top: -60px;
  font-size: 68px;
  text-align: center;
}

    </style>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url()?>">DETEKSI DINI COVID-19 ONLINE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
     
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

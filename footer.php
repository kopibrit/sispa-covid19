<div class="container">

<footer>
<p class="p-footer"><?php echo date('Y');?> - Deteksi Dini Covid-19</p>
</footer>

</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.simpleLoadMore.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
    <script>
    $(document).ready(function(){
        getGlobalInfo();
    });
    

    function format3(n, currency) {
        return new Intl.NumberFormat({
        style: 'currency',
        currency: currency
    }).format(n);
    }
    function getGlobalInfo(){
        $.ajax({
        url : 'https://coronavirus-19-api.herokuapp.com/countries/indonesia',
        
        success: function (data) {
            try {
                var json = data;

                var kasus = json.cases;
                var meninggal = json.deaths;
                var pulih = json.recovered;
                var rawat = json.active;

                $("#global-kasus").html(format3(kasus));
                $("#global-meninggal").html(format3(meninggal));
                $("#global-pulih").html(format3(pulih));
                $("#global-rawat").html(format3(rawat));
            }catch(e){
                alert("Data Gagal diambil");
            }
        }, error: function(resp) {
            
        }
        });
    }

$('.some-list-2').simpleLoadMore({
      item: 'div',
      count: 15,
      itemsToLoad: 5
    });

$(document).ready(function () {
        $('.tanggal').datepicker({   
        format: "dd-mm-yyyy",
        autoclose:true
     });
});
</script>
</body>
</html>
<?php 
error_reporting(0);
session_start();

?>
<?php include 'header.php';?>
<div class="container">
  
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6">
  <form name="form" action="proses.php" method='POST'>
  <h3>Identitas Diri</h3>
  <div class="form-group">
    <label for="nama">Nama Lengkap</label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
  </div>
  <div class="form-group">
    <label>Jenis Kelamin</label>
      <select class="form-control" name="jenis_kelamin" id="jenis_kelamin" required="">
      <option value="">Pilih</option>
      <option value="Laki-Laki">Laki-Laki</option>
      <option value="Perempuan">Perempuan</option>
      </select>
  </div>
  <div class="form-group">
                <label for="tanggal">Tanggal Lahir</label>
                <input type="text" name="tanggal" class="form-control tanggal" id="tanggal"/>
  </div>						
								
    <div class="form-group">
    <label for="lokasi">Nomor WhatsApp</label>
    <input type="text" class="form-control" id="nowa" name="nowa" placeholder="ex : 82394571505" required>
  </div>
  
    <h3>Alamat Sekarang / Tinggal</h3>
    <div class="form-group">
    <label for="lokasi">Propinsi</label>
    <input type="text" class="form-control" id="lokasi" name="provinsi" placeholder="Lokasi" required>
  </div>
      <div class="form-group">
    <label for="lokasi">Kota / Kabupaten</label>
    <input type="text" class="form-control" id="lokasi" name="kokab" placeholder="Lokasi" required>
  </div>
      <div class="form-group">
    <label for="lokasi">Kecamatan</label>
    <input type="text" class="form-control" id="lokasi" name="kecamatan" placeholder="Lokasi" required>
  </div>
      <div class="form-group">
    <label for="lokasi">Desa Kelurahan</label>
    <input type="text" class="form-control" id="lokasi" name="kelurahan" placeholder="Lokasi" required>
  </div>
      <div class="form-group">
    <label for="lokasi">Alamat</label>
    <input type="text" class="form-control" id="lokasi" name="alamat" placeholder="Lokasi" required>
  </div>
  <br>
  <p>Keterangan :</p>
  <ul>
    <li>Nilai Score : 0-10 (OTG atau Orang Tanpa Gejala atau Sehat )</li>
    <li>Nilai Score : 11-20 (ODP : Orang Dalam Pengawasan)</li>
    <li>Nilai Score : 21-31 (PDP : Pasien Dalam Pengawasan)</li>
  </ul>
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
  <h3>Deteksi Gejala</h3>
    <!-- Pertanyaan 1 -->
   <h3 class="text-title">1. Apakah anda merasa Demam / riwayat demam (suhu ? 38 Derajat Celcius) ?</h3>
   <div class="b-radio">
    <label class="radio-inline">
    <input type="radio" name="g01" id="g01" value="3"> Ya
    </label>
    <label class="radio-inline">
    <input type="radio" name="g01" id="g01" value="0"> Tidak
    </label>
    </div>
    
      <!-- Pertanyaan 2 -->
   <h3 class="text-title">2. Apakah Anda Batuk ?</h3>
   <div class="b-radio">
    <label class="radio-inline">
    <input type="radio" name="g02" id="g02" value="1"> Ya
    </label>
    <label class="radio-inline">
    <input type="radio" name="g02" id="g02" value="0"> Tidak
    </label>
    </div>
    <!-- Pertanyaan 3 -->
   <h3 class="text-title">3. Apakah Anda Pilek ?</h3>
   <div class="b-radio">
    <label class="radio-inline">
    <input type="radio" name="g03" id="g03" value="1"> Ya
    </label>
    <label class="radio-inline">
    <input type="radio" name="g03" id="g03" value="0"> Tidak
    </label>
    </div>
    
    <!-- Pertanyaan 4 -->
    <h3 class="text-title">4. Apakah Anda Nyeri Tenggorokan ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g04" id="g04" value="1"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g04" id="g04" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 5 -->
    <h3 class="text-title">5. Apakah anda merasa sesak nafas atau kesulitan bernafas ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g05" id="g05" value="1"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g05" id="g05" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 6 -->           
    <h3 class="text-title">6. Apakah anda merasa nyeri otot ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g06" id="g06" value="1"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g06" id="g06" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 7 -->            
    <h3 class="text-title">7. Apakah anda merasa nyeri kepala ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g07" id="g07" value="1"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g07" id="g07" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 8 -->            
    <h3 class="text-title">8. Apakah ada keluhan saluran pencernaan ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g08" id="g08" value="1"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g08" id="g08" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 9 -->            
    <h3 class="text-title">9. Apakah anda memiliki Riwayat perjalanan ke luar negeri dalam waktu 14 hari sebelum timbul gejala ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g09" id="g09" value="5"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g09" id="g09" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 10 -->            
    <h3 class="text-title">10. Apakah anda memiliki Riwayat perjalanan ke kota terjangkit di Indonesia dalam waktu 14 hari sebelum timbul gejala, Kota – kota terjangkit ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g10" id="g10" value="5"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g10" id="g10" value="0"> Tidak 
        </label>
    </div>
    
    <!-- Pertanyaan 11 -->            
    <h3 class="text-title">11. Apakah Ada riwayat kontak erat dengan kasus konfirmasi COVID-19 ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g11" id="g11" value="5"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g11" id="g11" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 12 -->            
    <h3 class="text-title">12. Apakah Anda bekerja atau pernah mengunjungi fasilitas kesehatan yang berhubungan dengan pasien konfirmasi COVID-19 ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g12" id="g12" value="3"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g12" id="g12" value="0"> Tidak
        </label>
    </div>
    
    <!-- Pertanyaan 13 -->            
    <h3 class="text-title">13. Apakah Anda memiliki riwayat kontak dengan hewan penular (jika hewan penular sudah teridentifikasi) ?</h3>
    <div class="b-radio">
        <label class="radio-inline">
        <input type="radio" name="g13" id="g13" value="3"> Ya
        </label>
        <label class="radio-inline">
        <input type="radio" name="g13" id="g13" value="0"> Tidak
        </label>
    </div>
  <button type="submit" class="btn btn-default"  name="kirim">Kirim</button>
</form>
  </div>
</div>

</div>
<?php include 'footer.php';?>